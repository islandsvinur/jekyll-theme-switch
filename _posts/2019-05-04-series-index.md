---
layout: series_index
series_slug: first-series
title: First series
---

This is an **index post** to introduce the series. It contains a table of
contents of other posts in the same series. This is the Front Matter:

```
layout: series_index
series_slug: first-series
title: First series
```

Suspendisse suscipit, sapien ac venenatis sodales, enim libero cursus felis, vel
volutpat lectus lorem quis quam. Class aptent taciti sociosqu ad litora torquent
per conubia nostra, per inceptos himenaeos. Curabitur in magna hendrerit, porta
nulla eu, molestie nulla. Nulla consectetur venenatis urna eu vestibulum. In nec
eleifend lorem. Proin luctus eros felis, sit amet euismod augue faucibus
convallis. Donec eget dui dictum, euismod ante vel, hendrerit mi. Praesent id
semper tellus. Aenean ac libero convallis, sagittis lacus a, laoreet nunc. Donec
vitae sem ullamcorper, mattis justo in, tristique est.

Vivamus bibendum sem libero, in elementum elit blandit nec. Donec dolor nunc,
gravida eget dignissim eu, auctor nec tortor. Nullam eleifend, ex eget ornare
maximus, mi enim congue leo, a aliquam sapien neque ut mi. Praesent non bibendum
nisi. Curabitur bibendum enim ac diam varius suscipit. Curabitur et mauris nec
nibh iaculis rhoncus sit amet ac enim. Etiam eu ex vitae arcu commodo ornare
luctus vel augue. Nullam a gravida orci. Vivamus sit amet odio vel neque rhoncus
malesuada. Fusce euismod odio ac ligula commodo, vitae suscipit eros imperdiet.
Duis ullamcorper ante dui, nec dapibus quam vestibulum non. Pellentesque
habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
Duis quis tristique leo, non maximus augue. Sed sit amet nibh id orci elementum
ullamcorper.


