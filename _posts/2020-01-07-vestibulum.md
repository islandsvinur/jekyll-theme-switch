---
layout: post
title: Vestibulum et neque mattis, sollicitudin elit id, tincidunt elit
lead_image: /assets/image/fabian-betto-d3npqyXkaGI-unsplash.jpg
lead_image_credit:
    unsplash:
        name: Fabian Betto
        username: fabianbetto
---
Vestibulum et neque mattis, sollicitudin elit id, tincidunt elit. Sed accumsan massa ac quam vehicula, eget porttitor
dui blandit. Fusce auctor rhoncus ultricies. Cras suscipit, augue sed tempus pellentesque, odio est fringilla ante, id
lacinia est dolor vitae nulla. <q>Donec id magna velit. Phasellus pellentesque odio sed turpis pulvinar pretium.</q> Sed
ornare nunc eros, eu sodales mauris gravida vel.

![Single tree in a snowy landscape]({% link assets/image/fabian-betto-d3npqyXkaGI-unsplash.jpg %}){: .left width="200px"} Praesent vel
fringilla dui. Etiam semper nisl ut lacus viverra molestie. Nulla eget pharetra nulla. Donec a tempor turpis, id lacinia
quam. Integer sed massa nec ante congue lobortis sed eget enim. Fusce semper ut nunc in commodo. Fusce tortor sem,
efficitur a fringilla eget, hendrerit a arcu. Vivamus ut rhoncus urna, sed consequat felis.

{% raw %}
```markdown
![Single tree in a snowy landscape]({% link assets/image/fabian-betto-d3npqyXkaGI-unsplash.jpg %}){: .left width="200px"}
```
{% endraw %}

## 75% width, centered

![Single tree in a snowy landscape]({% link assets/image/fabian-betto-d3npqyXkaGI-unsplash.jpg %}){: .width-75}  
_Photo by [Fabian Betto](https://unsplash.com/@fabianbetto)_
{: .center}

{% raw %}
```markdown
![Single tree in a snowy landscape]({% link assets/image/fabian-betto-d3npqyXkaGI-unsplash.jpg %}){: .width-75}
_Photo by [Fabian Betto](https://unsplash.com/@fabianbetto)_
{: .center}
```
{% endraw %}

## 50% width, centered

![Willemswerf in Rotterdam]({% link assets/image/arnaud-jaegers-OkXIepDkNBE-unsplash.jpg %}){: .width-50}  
_Photo by [Arnaud Jaegers](https://unsplash.com/@ajaegers)_
{: .center}

{% raw %}
```markdown
![Willemswerf in Rotterdam]({% link assets/image/arnaud-jaegers-OkXIepDkNBE-unsplash.jpg %}){: .width-50}  
_Photo by [Arnaud Jaegers](https://unsplash.com/@ajaegers)_
{: .center}
```
{% endraw %}
