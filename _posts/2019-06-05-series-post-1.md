---
layout: series_post
series: first-series
title: Series post
---

This is a series **post**. This is the Front Matter:

```
layout: series_post
series: first-series
title: Series post
```

Morbi efficitur turpis vel cursus mattis. Suspendisse sed maximus metus, eget
sodales ex. Aliquam ut diam diam. Ut ut varius turpis, a sollicitudin orci.
Phasellus vel odio mollis urna ullamcorper vehicula eget ac est. Duis posuere
euismod augue in ornare. Suspendisse eu eros risus. Nulla sed consequat elit.
Pellentesque feugiat molestie augue, vitae fringilla lacus. Orci varius natoque
penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum
mauris tortor, mollis in mi id, cursus mollis est. Phasellus ultrices dapibus
sollicitudin.

Aenean at consequat urna. Phasellus sit amet congue eros. Nulla dapibus et dui
non congue. Cras mauris nulla, tempor sodales dolor eu, ultricies finibus ipsum.
Morbi volutpat sollicitudin odio. Fusce vitae elit iaculis, blandit neque eget,
pharetra nibh. Donec quis sem commodo, tristique lacus ut, scelerisque dolor.
Fusce tempus quam ac erat condimentum, et tincidunt odio iaculis.

Vivamus vitae velit eget sem iaculis mattis vitae vitae massa. Cras vehicula
odio euismod elementum ultrices. Phasellus a consequat arcu. Suspendisse
pellentesque placerat mauris, sed venenatis purus ullamcorper sit amet. Nullam
convallis nibh quis urna consectetur, in faucibus arcu tristique. Cras vitae
diam risus. Nulla non est purus. Aenean euismod fringilla ullamcorper. Duis ut
risus rhoncus, molestie neque nec, scelerisque nulla. Etiam vel accumsan felis.
Nunc eget nibh iaculis, molestie eros at, luctus erat. Nunc ornare mi sed eros
mattis, quis efficitur diam iaculis. Vestibulum venenatis efficitur ante sed
rhoncus. Maecenas convallis lorem feugiat nisi bibendum viverra.
