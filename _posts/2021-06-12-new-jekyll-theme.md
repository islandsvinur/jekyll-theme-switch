---
layout: post
modified_date: 2023-01-31
title: New Jekyll theme!
lead_image: /assets/image/marek-piwnicki-NPct-Mxw-64-unsplash.jpg
lead_image_credit:
    unsplash:
        name: Marek Piwnicki
        username: marekpiwnicki
---
This is the demo page of a new Jekyll theme.

You probably don't want to, but if you really must use it, this is how.

Add the gem to your `Gemfile`:

```ruby
gem 'jekyll-theme-switch', ref: 'v0.7.0'
```

Run `bundle install`, then set the theme in your `_config.yml`:

```
theme: jekyll-theme-switch
```
