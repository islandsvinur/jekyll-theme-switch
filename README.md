# jekyll-theme-switch

This is the Jekyll theme that powers the [Logging the Switch](https://luijten.org/) blog and is created by the same
author. It has various interesting features and is customizable to some extent; you might want to consider it.

## Installation

Add the gem to your `Gemfile` (it's not on rubygems.org yet):

```ruby
git 'https://gitlab.com/islandsvinur/jekyll-theme-switch.git' do
  gem 'jekyll-theme-switch', ref: 'v0.5.0'
end
```

Run `bundle install`, then set the theme and add the plugin in your `_config.yml`:

```
theme: jekyll-theme-switch
plugins:
    - jekyll-theme-switch
```

## Usage

TODO: Write usage instructions here. Describe your available layouts, includes, sass and/or assets.

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/islandsvinur/jekyll-theme-switch. This project
is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to
the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## Development

To set up your environment to develop this theme, run `bundle install`.

Your theme is setup just like a normal Jekyll site! To test your theme, run `bundle exec jekyll serve` and open your
browser at `http://localhost:4000`. This starts a Jekyll server using your theme. Add pages, documents, data, etc. like
normal to test your theme's contents. As you make modifications to your theme and to your content, your site will
regenerate and you should see the changes in the browser after a refresh, just like normal.

When your theme is released, only the files in `_layouts`, `_includes`, `_sass` and `assets` tracked with Git will be
bundled. To add a custom directory to your theme-gem, please edit the regexp in `jekyll-theme-switch.gemspec`
accordingly.

## License

The theme is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

