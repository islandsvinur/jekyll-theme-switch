# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = "jekyll-theme-switch"
  spec.version       = "0.7.1"
  spec.authors       = ["Christian Luijten"]
  spec.email         = ["christian@luijten.org"]

  spec.summary       = "The theme for Logging the Switch."
  spec.homepage      = "https://islandsvinur.gitlab.io/jekyll-theme-switch/"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r!^(assets/css|css|_data|_layouts|_includes|_plugins|_sass|LICENSE|README)!i) }

  spec.add_runtime_dependency "jekyll", "~> 4.0"

  spec.add_development_dependency "rake", "~> 12.0"

  spec.add_runtime_dependency "jekyll-paginate", "~> 1.1.0"
  spec.add_runtime_dependency "jekyll-gist", "~> 1.5.0"
  spec.add_runtime_dependency "jekyll-archives", "~> 2.2.1"
  spec.add_runtime_dependency "jekyll-textile-converter", "~> 0.1.0"
  spec.add_runtime_dependency "jekyll-youtube", "~> 1.0.0"
  spec.add_runtime_dependency "jekyll-sitemap", "~> 1.4.0"
end
