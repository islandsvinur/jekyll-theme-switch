# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# [Unreleased]

### Added

- Added `modified_date` in RSS feed item's `pubDate`

### Changed

- Changed the padding of the index pages, so posts take up the full width of the wrapper

### Deprecated
### Removed
### Fixed

- Fixed z-index of 'READ MORE' button in dark mode, which caused the background to bleed in

### Security

# [0.7.1] - 2024-02-21

### Fixed

- Removed `assets/image` from gemspec, shrinking the gem from 8 MB to 22 kB

# [0.7.0] - 2024-02-21

### Added

- Added Mastodon profile link. Configure `mastodon_profile` with your handle, e.g. `@islandsvinur@mastodon.nl`. Adds
  the `rel="me"` attribute, so your blog will get a green check mark on your Mastodon profile!
- Added plugins to theme gem, now they actually work when you use the theme. Add `jekyll-theme-switch` to your `plugins`
  in `_config.yml`.
  - Blog series support. Define a `series_slug` header in a series index post, add `series` headers referring to the
    slug to series posts
  - Tags:
    - `category_link` creates a link to the page of the given category
    - `video` inserts a HTML5 video player for the given file
    - `vimeo` inserts a Vimeo player for the given URL
  - Filters:
    - `filesize` returns the size in bytes of the given file
    - `mastodon_url` returns the Mastodon profile url of the given handle, e.g. `@islandsvinur@mastodon.nl` turns into https://mastodon.nl/@islandsvinur
    - `normalize_whitespace` replaces all repeated whitespace into a single space
    - `songlink` creates a [Songlink](https://song.link/) embed based on its Songlink
- Added the date a post was modified to the post's header, based on the `modified_date` field of the post
- Added the ability to change the `<channel>` element of the RSS feed, by overriding the `_includes/rss/channel.html` file
- Added the configuration option `site_repository_base_url` which is used to construct an 'Edit this page' footer text
- Added the ability to translate certain strings, by overriding the `_data/i18n.yaml` file
- Added overridable post navigation in `_includes/post_nav.html`
- Added full post contents in index, with shadow to hide the overflow

### Changed

- Changed the homepage to a responsive [Material Design](https://m2.material.io) card list
- Moved the `<head>` tag from `_includes/head.html` to `_layouts/default.html`, so uses can easily extend the head section
- Changed tag and category index templates use the same template (overridable through `_includes/post_list.html` and `_includes/post_item.html`) as the home page
- Changed the homepage back to a normal [Material Design](https://m2.material.io) card list

### Fixed

- Fixed background of posts in dark mode
- Fixed an off-by-one error in printing month names

# [0.6.0] - 2023-03-28

### Added

- Added styles for image layouts
- Added styles for embed containers (YouTube, Vimeo, iframe etc.)

### Changed

- Changed lead image background image to radial-gradient instead of linear-gradient

### Fixed

- Use `Jekyll::Utils.slugify` for `taglinks` and `categorylinks` filters

### Security

# [0.5.1] - 2022-12-15

### Added

* Added 'subtitle' support for pages and posts
* Added styling of lead image credit
* Added styling of <q> tags

### Removed

* Removed 'Posts' heading from homepage

### Fixed

* Fixed color and fill of metadata icon and text in dark mode
* Use flex gaps instead of margins for header metadata items in lead image
* Back-ported styling of series headers and footers

# [0.5.0] - 2022-09-18

### Added

* Added 'filesize' filter to get the size of a file in bytes
* Added 'length' attribute to RSS enclosure
* Added 'rss/post.html' include to enable overriding post content

### Changed

* Redesign of post/page metadata header

# [0.4.0] - 2021-06-12

### Added

* Added metadata for Twitter Cards
* Added configuration option `instagram_username` for Instagram link

### Changed

* Changed background-size of lead image to 'cover' on category and tag pages
* Make title in lead image header dependent on screen width

### Fixed

* Fixed colours of table rows in dark mode
* Fixed handling of whitespace in site description for non-markup locations (e.g. `<meta description>`) 

# [0.3.1] - 2021-05-24

### Fixed

* Fixed colours of table rows in dark mode

# [0.3.0] - 2021-05-24

### Added

* Added support for Markdown in the site description
* Added RSS feed layout with lead image as enclosure
* Added demonstration pages for Series posts

# [0.2.2] - 2021-03-28

### Changed

* Swapped post meta and post title in lead image header

### Fixed

* Fixed mobile horizontal scrolling bug for posts with lead image

# [0.2.1] - 2021-03-27

### Fixed

* Improved readability of headers on lead image

# [0.2.0] - 2021-03-26

### Added

* Added really nice full-width Lead Image header above posts, if Lead Image is available

### Fixed

* Removed erroneous dev dependency of 'bundler' from the Gemspec

# [0.1.7] - 2021-03-25

### Added

* Added support for Dark Mode in combination with Lead Images

### Fixed

* Fixed various URLs that did not have the `--baseurl` prepended 

## [0.1.6] - 2020-12-15

### Changed

* Navigation buttons improved

## [0.1.5] - 2020-11-26

### Fixed

* Fixed version number in Gemspec

## [0.1.4] - 2020-11-26 [YANKED]

### Added

* Added `links` configuration option to set HTML Resource Links
* Added `<article>` tags in category and tag index

### Removed

* Removed Disqus tracking cookie counter

### Fixed

* Fixed background color of syntax highlighting

## [0.1.3] - 2020-09-27

### Changed

* Dark mode colours to better fit accessibility advise

## [0.1.2] - 2020-09-26

### Fixed

* Syntax highlighting in dark mode

### Security

## [0.1.1] - 2020-03-02

Various bugfixes

## [0.1.0] - 2020-02-24

Initial release
