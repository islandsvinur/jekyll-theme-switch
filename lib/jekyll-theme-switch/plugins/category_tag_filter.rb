
module Jekyll
    module CategoryTagFilter
        def taglinks(input)
            site = @context.registers[:site]
            input.map do |tag|
                "<a href=\"#{site.baseurl}/#{site.config['tag_dir']}/#{Utils.slugify(tag)}\">#{tag}</a>"
            end
        end

        def categorylinks(input)
            site = @context.registers[:site]
            input.map do |cat|
                "<a href=\"#{site.baseurl}/#{site.config['category_dir']}/#{Utils.slugify(cat)}\">#{cat}</a>"
            end
        end
    end

    class CategoryLinkTag < Liquid::Tag
        def initialize(tag_name, text, tokens)
            super
            @text = text
        end

        def render(context)
            @context = context
            site = context.registers[:site]
            dir = site.config['category_dir'] || 'categories'

            "/#{dir}/#{@text}"
        end
    end
end

Liquid::Template.register_tag('category_link', Jekyll::CategoryLinkTag)
Liquid::Template.register_filter(Jekyll::CategoryTagFilter)
