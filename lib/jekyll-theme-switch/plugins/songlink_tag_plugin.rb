
module Jekyll
  class SonglinkTag < Liquid::Tag
    @songlink = ''

    def initialize(tag_name, markup, tokens)
        @songlink = markup.split(' ').first
    end

    def render(context)
        output = super
        url = "https://embed.song.link/?url=#{@songlink}&theme=light"
        songlink = "<iframe width='100%' height='414' src='#{url}' frameborder='0' allowfullscreen sandbox='allow-same-origin allow-scripts allow-presentation allow-popups allow-popups-to-escape-sandbox'></iframe>"
    end
  end
end

Liquid::Template.register_tag('songlink', Jekyll::SonglinkTag)
