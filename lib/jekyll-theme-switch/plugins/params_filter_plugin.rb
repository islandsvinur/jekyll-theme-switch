module Jekyll
  module ParamsFilter
    def params(input, params = {})
      input.gsub(/%\{(\w+)\}/) { |_match| params[Regexp.last_match(1)] }
    end
  end
end

Liquid::Template.register_filter(Jekyll::ParamsFilter)