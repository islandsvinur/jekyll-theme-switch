
module Jekyll
  module NormalizeWhitespaceFilter
    def normalize_whitespace(input)
      input.gsub(/\s+/, ' ')
    end
  end
end

Liquid::Template.register_filter(Jekyll::NormalizeWhitespaceFilter)
