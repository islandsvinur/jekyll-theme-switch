

Jekyll::Hooks.register :site, :pre_render do |site|

  seriesIndexes = site.posts.docs
    .select { |p| p.data.key? 'series_slug' }
    .reduce(Hash.new {|h,k| h[k] = []} ) { |series, post| series[post.data['series_slug']] << post; series }

  seriesPosts = site.posts.docs
    .select { |p| p.data.key? 'series' }
    .reduce(Hash.new {|h,k| h[k] = []}) { |series, post| series[post.data['series']] << post; series }

  seriesIndexes.each { |slug, posts| posts.each {|post| post.data['series_posts'] = seriesPosts[slug] }}
  seriesPosts.each { |slug, posts| posts.each {|post| post.data['series_index'] = seriesIndexes[slug] }}
end

