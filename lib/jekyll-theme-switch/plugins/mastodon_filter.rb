
module Jekyll
  module MastodonFilter
    def mastodon_url(profile)
      profile.gsub(/@([^@]+)@([^@]+)/, 'https://\2/@\1')
    end
  end
end

Liquid::Template.register_filter(Jekyll::MastodonFilter)
