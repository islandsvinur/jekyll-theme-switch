
class VimeoEmbed < Liquid::Tag

  def initialize(tagName, content, tokens)
    super
    @content = content
    content[/([0-9]+)/]
    @vimeo_id = $1
  end

  def render(context)
    tmpl_path = File.join Dir.pwd, "_includes", "vimeo.html"
    if File.exist?(tmpl_path)
      tmpl = File.read tmpl_path
      site = context.registers[:site]
      tmpl = (Liquid::Template.parse tmpl).render site.site_payload.merge!({"vimeo_id" => @vimeo_id})
    else
      %Q{<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'>    <iframe title="vimeo video player" width="640" height="390" src="//www.vimeo.com/embed/#{ @vimeo_id }" frameborder="0" allowfullscreen></iframe></div>}
    end
  end

  Liquid::Template.register_tag "vimeo", self
end
